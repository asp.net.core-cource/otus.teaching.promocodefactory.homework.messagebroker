﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Messages;
using Rabbitmq;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class AdministrationGateway
        : IAdministrationGateway
    {
        private readonly HttpClient _httpClient;

        public AdministrationGateway(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }
        public async Task NotifyAdminAboutPartnerManagerPromoCode(Guid partnerManagerId, IOptions<RabbitMqSettings> rabbitOptions)
        {
            var rabbitmqHelper = new RabbitMqHelper<NotifyAdminAboutPartnerManagerPromoCodeMessage>(rabbitOptions);
            rabbitmqHelper.Producer.Produce(partnerManagerId);
        }
    }
}