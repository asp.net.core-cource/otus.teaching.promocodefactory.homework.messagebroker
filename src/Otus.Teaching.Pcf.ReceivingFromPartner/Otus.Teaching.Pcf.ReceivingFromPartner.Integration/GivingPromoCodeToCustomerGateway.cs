﻿using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Messages;
using Rabbitmq;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class GivingPromoCodeToCustomerGateway
        : IGivingPromoCodeToCustomerGateway
    {
        private readonly HttpClient _httpClient;

        public GivingPromoCodeToCustomerGateway(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }
        
        public async Task GivePromoCodeToCustomer(PromoCode promoCode, IOptions<RabbitMqSettings> rabbitOptions)
        {
            var dto = new GivePromoCodeToCustomerDto()
            {
                PartnerId = promoCode.Partner.Id,
                BeginDate = promoCode.BeginDate.ToShortDateString(),
                EndDate = promoCode.EndDate.ToShortDateString(),
                PreferenceId = promoCode.PreferenceId,
                PromoCode = promoCode.Code,
                ServiceInfo = promoCode.ServiceInfo,
                PartnerManagerId = promoCode.PartnerManagerId
            };

            var rabbitmqHelper = new RabbitMqHelper<GivePromoCodeToCustomerMessage>(rabbitOptions, routingKey: "promocode.2");
            rabbitmqHelper.Producer.Produce(dto, "promocode.2");
        }
    }
}