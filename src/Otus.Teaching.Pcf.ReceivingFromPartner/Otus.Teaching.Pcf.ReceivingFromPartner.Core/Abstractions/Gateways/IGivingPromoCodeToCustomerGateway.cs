﻿using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Rabbitmq;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways
{
    public interface IGivingPromoCodeToCustomerGateway
    {
        Task GivePromoCodeToCustomer(PromoCode promoCode, IOptions<RabbitMqSettings> rabbitOptions);
    }
}