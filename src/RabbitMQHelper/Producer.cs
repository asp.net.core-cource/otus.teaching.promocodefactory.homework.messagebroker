﻿using Rabbitmq;
using RabbitMQ.Client;
using System.Text;
using System.Text.Json;

namespace RabbitMQ
{
	public class Producer<TMessage>
        where TMessage : BaseMessage, new()
    {
        private IModel _channel;
        private string _exchangeName;
        private string _exchangeType;
        public Producer(IModel channel, string exchangeName, string exchangeType)
		{
            _exchangeName = exchangeName;
            _exchangeType = exchangeType;
            _channel = channel;
        }
        public void Produce(object content, string routingKey = "promocode.1")
        {
            _channel.ExchangeDeclare(_exchangeName, _exchangeType);

            var message = new TMessage()
            {
                Content = content
            };
            var body = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(message));

            _channel.BasicPublish(exchange: _exchangeName,
                routingKey: routingKey,
                basicProperties: null,
                body: body);
        }
    }
}
