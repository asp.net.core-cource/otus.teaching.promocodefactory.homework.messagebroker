﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rabbitmq
{
    public class BaseMessage
    {
        public object Content { get; set; }
    }
}
