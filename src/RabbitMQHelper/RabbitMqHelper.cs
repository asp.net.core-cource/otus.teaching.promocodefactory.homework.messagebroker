﻿using Microsoft.Extensions.Options;
using RabbitMQ;
using RabbitMQ.Client;
using System.Text;
using System.Text.Json;

namespace Rabbitmq
{
    public class RabbitMqHelper<TMessage>
        where TMessage : BaseMessage, new()
    {
        public Producer<TMessage> Producer { get; private set; }

		public RabbitMqHelper(IOptions<RabbitMqSettings> rabbitOptions, string exchangeName = "exchange.direct",
            string queueName = "demo.queue.log", string exchangeType = ExchangeType.Direct, string routingKey = "promocode.1")
		{
            var channel = InitRabbitMQ(rabbitOptions.Value, exchangeName, exchangeType, queueName, routingKey);
            Producer = new Producer<TMessage>(channel, exchangeName, exchangeType);
        }

        private IModel InitRabbitMQ(RabbitMqSettings rabbitSetting, string exchangeName, string exchangeType, string queueName, string routingKey)
        {
            ConnectionFactory factory = new ConnectionFactory
            {
                UserName = rabbitSetting.UserName,
                Password = rabbitSetting.Password,
                HostName = rabbitSetting.Host,// "host.docker.internal", 
                //Port = 5672
            };

            // create connection  
             var connection = factory.CreateConnection();

            // create channel  
            var channel = connection.CreateModel();

            channel.ExchangeDeclare(exchangeName, exchangeType);
            channel.QueueDeclare(queueName, false, false, false, null);
            channel.QueueBind(queueName, exchangeName, routingKey, null);
            channel.BasicQos(0, 1, false);

            connection.ConnectionShutdown += RabbitMQ_ConnectionShutdown;

            return channel;
        }
        private void RabbitMQ_ConnectionShutdown(object sender, ShutdownEventArgs e) { }
    }
}
