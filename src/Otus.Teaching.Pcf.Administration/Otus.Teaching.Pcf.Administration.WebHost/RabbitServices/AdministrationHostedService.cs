﻿using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Otus.Teaching.Pcf.Administration.Core.Services;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Messages;
using Rabbitmq;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Rabbit
{
	public class AdministrationHostedService : ConsumeRabbitMQHostedService
	{
        private readonly IServiceProvider _serviceProvider;

        public AdministrationHostedService(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        protected override async Task HandleMessageAsync(string message)
        {
            var content = JsonConvert.DeserializeObject<NotifyAdminAboutPartnerManagerPromoCodeMessage>(message).Content;

            if (!Guid.TryParse(content.ToString(), out var id))
                return;

            using var scope = _serviceProvider.CreateScope();
            var updateAppliedPromocodesService = scope.ServiceProvider.GetService<UpdateAppliedPromocodesService>();
            await updateAppliedPromocodesService.UpdateAppliedPromocodesAsync(id);
        }
    }
}
