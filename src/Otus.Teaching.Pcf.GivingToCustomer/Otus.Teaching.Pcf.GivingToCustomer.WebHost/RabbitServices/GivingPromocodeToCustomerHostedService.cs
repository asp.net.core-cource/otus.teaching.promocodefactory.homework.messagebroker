﻿using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Services;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;
using Rabbitmq;
using System;
using System.Text.Json;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.RabbitServices
{
	public class GivingPromocodeToCustomerHostedService : ConsumeRabbitMQHostedService
	{
        private readonly IServiceProvider _serviceProvider;

        public GivingPromocodeToCustomerHostedService(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        protected override async Task HandleMessageAsync(string message)
        {
            var dto = JsonSerializer.Deserialize<GivePromoCodeToCustomerDto>(message);
            
            if (dto == null)
                return;

            var request = new GivePromoCodeRequest
            {
                PreferenceId = dto.PreferenceId,
                PromoCode = dto.PromoCode,
                BeginDate = dto.BeginDate,
                EndDate = dto.EndDate,
                PartnerId = dto.PartnerId,
                ServiceInfo = dto.ServiceInfo,
                PromoCodeId = dto.PromoCodeId
            };

            using var scope = _serviceProvider.CreateScope();
            var givePromoCodesToCustomersService = scope.ServiceProvider.GetService<GivePromoCodeToCustomerService>();

            var isGive = await givePromoCodesToCustomersService.GivePromoCodesToCustomersAsync(request);
            if (!isGive)
                return;
        }
    }
}
